create table persona (
    id identity not null,
    nombre varchar,
    apellido varchar,
    edad int
);

create table persona_direccion (
    id identity not null,
    id_persona int,
    id_direccion int
)

create table direccion (
    id identity not null,
    calle varchar,
    numero int
);


insert into persona 
(id, nombre, apellido, edad)
values (1, 'coco', 'coco', 42);
insert into persona 
(id, nombre, apellido, edad)
values (2, 'pepe', 'pepe', 32);
insert into persona 
(id, nombre, apellido, edad)
values (3, 'tito', 'tito', 25);

insert into direccion 
(id, calle, numero)
values (1, 'Libertador', 1500);
insert into direccion 
(id, calle, numero)
values (2, 'Dorrego', 3000);
insert into direccion 
(id, calle, numero)
values (3, '9 de Julio', 2000);

insert into persona_direccion
(id_persona, id_direccion)
values (1,1);
insert into persona_direccion
(id_persona, id_direccion)
values (1,2);
insert into persona_direccion
(id_persona, id_direccion)
values (2,2);
insert into persona_direccion
(id_persona, id_direccion)
values (2,3);
insert into persona_direccion
(id_persona, id_direccion)
values (3,3);
insert into persona_direccion
(id_persona, id_direccion)
values (3,1);