package com.somospnt.squiggly.controller;

import com.somospnt.squiggly.domain.Persona;
import com.somospnt.squiggly.repository.PersonaRepository;
import java.util.List;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonaController {
    
    private PersonaRepository personaRepository;

    public PersonaController(PersonaRepository personaRepository) {
        this.personaRepository = personaRepository;
    }
    
    @RequestMapping("/api/personas")
    public List<Persona> personas() {
        return (List<Persona>) personaRepository.findAll();
    }
    
}
