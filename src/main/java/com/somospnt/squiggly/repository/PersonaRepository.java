package com.somospnt.squiggly.repository;

import com.somospnt.squiggly.domain.Persona;
import org.springframework.data.repository.CrudRepository;

public interface PersonaRepository extends CrudRepository<Persona, Long>{
    
}
