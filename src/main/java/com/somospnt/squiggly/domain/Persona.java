package com.somospnt.squiggly.domain;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import lombok.Data;

@Data
@Entity
public class Persona implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nombre;
    private String apellido;
    private int edad;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "persona_direccion",
            joinColumns = @JoinColumn(name = "id_persona", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "id_direccion", referencedColumnName = "id"))
    private List<Direccion> direcciones;

}
